package com.questdemo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.questdemo.model.Person;
import com.questdemo.service.PersonService;
import com.questdemo.service.PersonServiceImpl;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonServiceImplIntegrationTest {

	@TestConfiguration
	static class PersonServiceImplTestContextConfiguration {

		@Bean
		public PersonService employeeService() {
			return new PersonServiceImpl();
		}
	}

	@Autowired
	PersonService personService;

	@Mock
	private PersonRepository personRepository;

	@Test
	public void whenAddOnePerson_ThenAssertRecordName() {

		Person jean = new Person("Jean", "111222AO", LocalDate.of(1988, 6, 24), "089999999");
		jean.setId(personService.createPerson(jean).getId());
		Person found = personService.getPerson(jean.getId());

		assertThat(found.getName()).isEqualTo(jean.getName());

	}

	@Test
	public void whenDeletePerson_ThenAssertRecordIsDeleted() {

		Person jean = new Person("Jean", "1112233", LocalDate.of(1988, 6, 24), "089999999");
		jean.setId(personService.createPerson(jean).getId());

		personService.deletePerson(jean);
		List<Person> found = personService.getAllPerson();

		assert (found.size() == 0);

	}

	@Test
	public void whenEditPerson_ThenAssertChanges() {
		Person jean = new Person("Jean", "111222AO", LocalDate.of(1988, 6, 24), "089999999");
		jean.setId(personService.createPerson(jean).getId());

		Person found = personService.getPerson(jean.getId());
		String newMobile = "089000000";
		found.setMobile(newMobile);

		personService.editPerson(found);

		found = personService.getPerson(jean.getId());

		assertThat(found.getMobile()).isEqualTo(newMobile);
	}

	@Test
	public void whenRetrieveAllRecords() {
		Person jean = new Person("Jean", "111111A0", LocalDate.of(1988, 6, 24), "089999999");
		Person jean1 = new Person("Jean1", "222222AO", LocalDate.of(1989, 6, 24), "089944444");
		Person jean2 = new Person("Jean2", "333333AO", LocalDate.of(1990, 6, 24), "08995555");
		Person jean3 = new Person("Jean3", "444444AO", LocalDate.of(1991, 6, 24), "08996666");

		personService.createPerson(jean);
		personService.createPerson(jean1);
		personService.createPerson(jean2);
		personService.createPerson(jean3);

		List<Person> people = personService.getAllPerson();

		assert (people.size() == 4);

	}

}
