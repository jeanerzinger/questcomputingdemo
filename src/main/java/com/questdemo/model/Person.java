package com.questdemo.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.questdemo.validator.AgeConstraint;
import com.questdemo.validator.MobileConstraint;
import com.questdemo.validator.PpsConstraint;

@Entity(name = "person")
public class Person {

	public Person(){}
	
	public Person(@NotEmpty(message = "Name is mandatory") @Size(max = 25) String name,
			@NotEmpty(message = "PPS is mandatory") String pps,
			@NotNull(message = "Date of birth is mandatory") LocalDate birthDate, String mobile) {
		super();
		this.name = name;
		this.pps = pps;
		this.birthDate = birthDate;
		this.mobile = mobile;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotEmpty(message = "Name is mandatory")
	@Size(max = 25)
	private String name;

	@NotEmpty(message = "PPS is mandatory")
	@PpsConstraint
	@Column(unique = true)
	private String pps;

	@NotNull(message = "Date of birth is mandatory")
	@AgeConstraint
	@Past
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthDate;

	@MobileConstraint
	private String mobile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPps() {
		return pps;
	}

	public void setPps(String pps) {
		this.pps = pps;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
