package com.questdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.questdemo.model.Person;

@Repository
@Component
public interface PersonRepository extends JpaRepository<Person, Long> {

}
