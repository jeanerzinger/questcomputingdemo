package com.questdemo.validator;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AgeValidator implements ConstraintValidator<AgeConstraint, LocalDate> {

	@Override
	public boolean isValid(LocalDate age, ConstraintValidatorContext context) {

		if (age != null) {
			return 16 < ChronoUnit.YEARS.between(age, LocalDate.now());
		}
		return false;
	}

}
