package com.questdemo.validator;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.questdemo.model.Person;
import com.questdemo.service.PersonService;

@Component
public class PpsValidator implements ConstraintValidator<PpsConstraint, String> {

	@Autowired
	PersonService personService;

	@Override
	public void initialize(PpsConstraint ppsConstraint) {
	}

	@Override
	public boolean isValid(String pps, ConstraintValidatorContext context) {
		try {
			List<Person> persons = personService.getAllPerson();
			if (null != pps && null != personService)
				for (Person person : persons) {
					if (person.getPps().equals(pps)) {
						return false;
					}
				}

		} catch (Exception e) {
			System.out.println(e);
		}
		return true;
	}

}
