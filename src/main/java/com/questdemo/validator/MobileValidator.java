package com.questdemo.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MobileValidator implements ConstraintValidator<MobileConstraint, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (value == null || value.isEmpty())
			return true;

		if (value.length() > 1 && value.substring(0, 2).equals("08"))
			return true;
		
		return false;
	}

}
