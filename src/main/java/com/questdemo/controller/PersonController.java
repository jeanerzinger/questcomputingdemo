package com.questdemo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.questdemo.model.Person;
import com.questdemo.service.PersonService;

@Controller
public class PersonController {

	private final PersonService personService;

	@Autowired
	public PersonController(PersonService personService) {
		this.personService = personService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String homePage(Model model) {
		return "main";
	}

	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public String addPerson(Model model) {
		model.addAttribute("person", new Person());
		return "form";
	}

	@RequestMapping(value = "/personDelete/{id}", method = RequestMethod.GET)
	public String deletePerson(Model model, @PathVariable(required = true, name = "id") Long id) {
		personService.deletePerson(id);
		model.addAttribute("person", personService.getAllPerson());
		return "list";
	}

	@RequestMapping(value = "/person", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute @Valid Person person, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "form";
		} else {
			personService.createPerson(person);
			model.addAttribute("person", personService.getAllPerson());
			return "list";
		}
	}

	@RequestMapping(value = { "/personEdit", "/personEdit/{id}" }, method = RequestMethod.GET)
	public String editPerson(Model model, @PathVariable(required = false, name = "id") Long id) {
		if (null != id) {
			model.addAttribute("person", personService.getPerson(id));
		} else {
			model.addAttribute("person", new Person());
		}
		return "form";
	}

	@RequestMapping(value = "/list")
	public String listPerson(Model model) {

		model.addAttribute("person", personService.getAllPerson());
		return "list";
	}
}
