package com.questdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.questdemo.model.Person;
import com.questdemo.repository.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Person createPerson(Person person) {
		try {
			return personRepository.save(person);
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			System.out.println("PPS Already exists");
		}
		return person;
	}

	@Override
	public Person getPerson(Long id) {
		return personRepository.getOne(id);
	}

	@Override
	public Person editPerson(Person person) {
		return personRepository.save(person);
	}

	@Override
	public void deletePerson(Person person) {
		personRepository.delete(person);

	}

	@Override
	public void deletePerson(Long id) {
		personRepository.deleteById(id);
	}

	@Override
	public List<Person> getAllPerson() {
		return personRepository.findAll();
	}

}
