package com.questdemo.service;

import java.util.List;

import com.questdemo.model.Person;

public interface PersonService {
	Person createPerson(Person person);

	Person getPerson(Long id);

	Person editPerson(Person person);

	void deletePerson(Person person);

	void deletePerson(Long id);

	List<Person> getAllPerson();
}
